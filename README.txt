Adresă repository: https://gitlab.upt.ro/bianca.bora/sistem-de-monitorizare-i-control-al-parametrilor-intr-o-camera-de-depozitare-a-componentelor-electronice


Proiectul de pe gitlab conține o arhivă unde se regăsesc urmîătoarele:
-software-ul pentru microcontroler (Atmega128A-AU) scris în limbajul C folosind mediul de programare CodeVision AVR;
-o interfață de descărcare și stocarea datelor achiziționate de microcontroler prin senzori, date care sunt stocate într-o memorie EEPROM 
externă. Această interfață a fost dezvoltată în limbajul C# folosind mediul Visual Studio 2019;

Partea hardware a proiectului este formată dintr-un PCB proiectat de noi care are următoarele componente:
-microcontroler Atmega128A-AU;
-sursă de 3.3V ce alimentează întreg sistemul, mai puțin partea de backlight a unui LCD, pentru aceasta s-a folosit o sursă suplimentară
de 5V;
-LCD pe care se afișează un meniu pentru interacțiunea utilizatorului cu sistemul;
-butoane pentru navigare prin opțiunile meniului;
-senzori pentru achiziționarea datelor (temperatură, umiditate, nivel apă, intensitate luminoasă);
-optotriac și triac pentru controlul unui bec alimentat de curent alternativ;
-optocuplor pentru a crea o barieră optică între microcontroler și o sarcină de tensiune mai mare;
-releu pentru acționarea unui ventilator ce reglează temperatura/umiditatea;
-buzzer pentru alarmarea utilizatorului în cazul în care s-au depășit nivelele de temperatura/umiditatea setate precum și în cazul în care
s-a detectat prezența apei, deoarece acest lucru reprezintă o posibilă inundație;
-memorie EEPROM pentru stocarea datelor;
-modul de ceas în timp real pentru contorizarea timpului la care s-au achiziționat datele;
-GPS pentru calibrarea automată a modulului de ceas la un interval de 24h;
-2 drivere de comunicație RS485 folosite pentru programare și descărcare de date la calculator;
-modul LoRa pentru descărcarea datelor prin intermediul radiofrecvenței;
-un ansamblu format dintr-un convertor UART-USB și un modul LoRa pentru descărcarea datelor;


